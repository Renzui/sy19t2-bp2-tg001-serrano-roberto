#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct Player
{
	int Dice;
	int Dice1;
};

int bet(int gold)
{

	return gold;
}

int bet(int &gold)
{

	return gold;
}


void bet(int &gold, int &bet)
{
	while (true)
	{
		cout << "Current Gold: " << gold << endl;
		cout << "Enter bet: ";
		cin >> bet;
		if (bet <= 0)
		{
			cout << "Your bet must be higher than 0! " << endl;
			system("pause");
			system("cls");
			continue;
		}
		else if (bet > gold)
		{
			cout << "Your bet must be lower! " << endl;
			system("pause");
			system("cls");
			continue;
		}

		gold -= bet;
		break;
	}
	system("pause");
	system("cls");
}

void Payout(int &gold, int &bet, Player &player, Player &ai)
{
	player.Dice = rand() % 6 + 1;
	player.Dice1 = rand() % 6 + 1;
	ai.Dice = rand() % 6 + 1;
	ai.Dice1 = rand() % 6 + 1;
	int AIbet = rand() % 200 + 1;
	int total = player.Dice + player.Dice1;
	int totalAI = ai.Dice + ai.Dice1;
	cout << "Player roll: ";
	cout << player.Dice << " " << player.Dice1 << endl;

	cout << "AI roll: ";
	cout << ai.Dice << " " << ai.Dice1 << endl;


	if (total > totalAI)
	{
		cout << "You win the bet! " << endl;
		cout << "AI bet: " << AIbet << endl;
		gold += AIbet;
		system("pause");
	}
	else if (total < totalAI)
	{
		cout << "You lost! AI wins the bet. " << endl;
		cout << "AI bet: " << AIbet << endl;
		system("pause");
	}
	else if (total == totalAI)
	{
		cout << "It's a draw!!" << endl;
		cout << "AI bet: " << AIbet << endl;
		gold == AIbet;
		system("pause");
	}
	else if (total == 2)
	{
		cout << "Snake Eyes!! " << endl;
		gold = (AIbet * 3) + gold;
		system("pause");
	}
	system("cls");
}

int main()
{
	srand(time(NULL));
	int Playergold = 1000;
	int Bet;
	Player player;
	Player ai;
	while (Playergold > 0)
	{
		bet(Playergold, Bet);
		Payout(Playergold, Bet, player, ai);
	}
	system("cls");
	cout << "You have no gold left!! Goodbye" << endl;
	system("pause");
	return 0;
}