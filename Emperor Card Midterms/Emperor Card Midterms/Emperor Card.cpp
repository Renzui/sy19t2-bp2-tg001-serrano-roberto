#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

void Deck(vector <string> &playerDeck, vector <string> &enemyDeck, int &wager, int &milimeter, int &cash, string &side)
{
	int cardPick;
	int tonegawaPick = rand() % 5 + 1;

	side = "Emperor";
	cout << "Pick a card to play..." << endl;
	cout << "======================" << endl;
	
	for (int i = 0; i < 5; i++)
	{
		if (i < 1)
		{
			playerDeck.push_back("Emperor");
			enemyDeck.push_back("Slave");
		}
		else
		{
			playerDeck.push_back("Citizen");
			enemyDeck.push_back("Citizen");

		}
		cout << "       [" << i + 1 << "] " << playerDeck.at(i) << endl;
	}
	cin >> cardPick;

	system("cls");

	cout << "Open!" << endl;
	cout << endl;

	if (cardPick == 1)
	{
		if (tonegawaPick == 1)
		{
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 3)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 4)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 5)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
	}
	if (cardPick == 2)
	{
		if (tonegawaPick == 1)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	if (cardPick == 3)
	{
		if (tonegawaPick == 1)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	if (cardPick == 4)
	{
		if (tonegawaPick == 1)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	if (cardPick == 5)
	{
		if (tonegawaPick == 1)
		{
			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	system("pause");
	system("cls");
}

void SlaveDeck(vector <string> &playerDeck, vector <string> &enemyDeck, int &wager, int &milimeter, int &cash, string &side)
{
	int cardPick;
	int tonegawaPick = rand() % 5 + 1;

	side = "Slave";
	cout << "Pick a card to play..." << endl;
	cout << "======================" << endl;

	for (int i = 0; i < 5; i++)
	{
		if (i < 1)
		{
			playerDeck.push_back("Slave");
			enemyDeck.push_back("Emperor");
		}
		else
		{
			playerDeck.push_back("Citizen");
			enemyDeck.push_back("Citizen");
		}
		cout << "       [" << i + 1 << "] " << playerDeck.at(i) << endl;
	}

	cin >> cardPick;
	system("cls");

	cout << "Open!" << endl;
	cout << endl;

	if (cardPick == 1)
	{
		if (tonegawaPick == 1)
		{
			cash += wager * 100000 * 5;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You won " << cash << " yen" << endl;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{

			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 3)
		{

			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 4)
		{

			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 5)
		{

			cash += wager * 100000;
			cout << "[Kaiji] " << playerDeck.at(0) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
	}
	if (cardPick == 2)
	{
		if (tonegawaPick == 1)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	if (cardPick == 3)
	{
		if (tonegawaPick == 1)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	if (cardPick == 4)
	{
		if (tonegawaPick == 1)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	if (cardPick == 5)
	{
		if (tonegawaPick == 1)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(0) << endl;
			cout << endl;
			cout << "You lost! The pin will now move by " << wager << "mm" << endl;
			milimeter -= wager;
			playerDeck.clear();
			enemyDeck.clear();
		}
		else if (tonegawaPick == 2)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(1) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 3)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(2) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 4)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(3) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
		else if (tonegawaPick == 5)
		{
			cout << "[Kaiji] " << playerDeck.at(1) << " vs " << "[Tonegawa] " << enemyDeck.at(4) << endl;
			cout << endl;
			cout << "Draw!" << endl;
			playerDeck.erase(playerDeck.begin() + cardPick);
			enemyDeck.erase(enemyDeck.begin() + tonegawaPick);
		}
	}
	system("pause");
	system("cls");
}

void Wager(int &round, int &cash, int &milimeter, int &wager, vector <string> &playerDeck, vector <string> &enemyDeck, string &side)
{
	for (int i = 0; i < round; i++)
	{
		cout << "Cash: " << cash << endl;
		cout << "Distance left (mm): " << milimeter << endl;
		cout << "Round " << round++ << "/" << "12" << endl;
		cout << "Side: " << side << endl;
		cout << endl;
		cout << "How many mm would you like to wager, Kaiji? ";
		cin >> wager;

		while (wager > milimeter || milimeter < 1)
		{
			cin >> wager;
		}
		system("cls");

		if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
		{
			Deck(playerDeck, enemyDeck, wager, milimeter, cash, side);
		}
		else if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
		{
			SlaveDeck(playerDeck, enemyDeck, wager, milimeter, cash, side);
		}
	}
}

int main()
{
	srand(time(NULL));
	int Cash = 0;
	int Milimeter = 30;
	int wager = 0;
	int Round = 1;
	string side = "Emperor";
	vector <string> playerDeck;
	vector <string> enemyDeck;

	for(int i = 0; i < 12; i++)
	{
		Wager(Round, Cash, Milimeter, wager, playerDeck, enemyDeck, side);

		if (Cash >= 20000000)
		{
			cout << "Congrats!! You won the game! " << endl;
			system("pause");
			system("cls");
			return 0;
		}
		else
		{
			cout << "You didn't reach the amount you want to earn, better luck next time. " << endl;
			system("pause");
			system("cls");
			return 0;
		}
		if (Milimeter >= 1)
		{
			cout << "You didn't lose your ear " << endl;
			system("pause");
			system("cls");
		}
		else
		{
			cout << "Your ears are now destroyed " << endl;
			system("pause");
			system("cls");
			return 0;
		}
	}
	system("pause");
	return 0;
}