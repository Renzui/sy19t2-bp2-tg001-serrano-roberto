#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};

int main()
{
	srand(time(NULL));

	Node *n1 = new Node;

	Node *n2 = new Node;
	n1->next = n2;

	Node *n3 = new Node;
	n2->next = n3;

	Node *n4 = new Node;
	n3->next = n4;

	Node *n5 = new Node;
	n4->next = n5;

	n5->next = n1;

	Node* current = n1;

	int totalSoldier = 5;
	for (int i = 0; i < totalSoldier; i++)
	{
		string soldier;
		cout << "What's your name soldier? ";
		cin >> soldier;
		current->name = soldier;
		//cout << current->name << endl;
		current = current->next;
	}

	Node* deleted;

	while (totalSoldier != 1)
	{
		int round = 1;
		int Members = 5;

		for (int i = 0; i < round; i++)
		{
		int cloak = rand() % 5 + 1;
			cout << "=====================" << endl;
			cout << "ROUND " << round++ << endl;
			cout << "=====================" << endl;
			cout << "Remaining Members:" << endl;
			for (int i = 0; i < Members; i++)
			{
				cout << current->name << endl;
				current = current->next;
			}

			cout << endl << endl;
			deleted = current->next;
			cout << "Result: " << endl;
			cout << current->name << " has drawn " << cloak << endl;

			for (int i = 0; i < cloak - 1; i++)
			{
				current = current->next;
			}

			current->next = current->next->next;
			cout << deleted->name << " was eliminated " << endl;
			current = current->next;
			Members--;

			system("pause");
			system("cls");
		}
		cout << "==========================================" << endl;
		cout << "FINAL RESULT" << endl;
		cout << "==========================================" << endl;
		cout << current->name << " will go to seek for reinforcements. " << endl;
		delete deleted;
	}

	system("pause");
	return 0;
};